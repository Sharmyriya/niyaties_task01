import React from 'react';
import { Caption, Appbar } from "react-native-paper";
import {View, StyleSheet,Dimensions,FlatList, Text}from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { AuthContext } from '../components/context';

const HomeScreen = () => {
  const { signOut } = React.useContext(AuthContext);
  const [status, setStatus] = React.useState(false)
  const [store, setStore] = React.useState({
    userName: '',
    userMobile: '',
    userEmail: ''
  });

  const [list, setList] = React.useState([{ 'name': 'AAAAAA', "email": 'Aaaa123@gmail.com' },
  { 'name': 'BBBBB', "email": 'BBBBB123@gmail.com' },
  { 'name': 'CCCCCCC', "email": '2CCCCCc3@gmail.com' },
  { 'name': 'DDDDDDD', "email": 'ddddd123@gmail.com' },
  { 'name': 'EEEEEEE', "email": 'eeeeeeee123@gmail.com' },
 ])

  // array data rendered
  const renderBay = ({ item }) => {
    return (
      <View style={{ flex: 1, marginTop: 10, borderRadius: 5, marginHorizontal: 30 }}>
        <View style={styles.slots}>
          <Caption style={{ fontWeight: "bold", fontSize: 12, color: "black" }}>
            Name: {item.name}
          </Caption>
          <Caption style={{ fontWeight: "bold", fontSize: 12, color: "black" }}>
            Email: {item.email}
          </Caption>
        </View>
      </View>
    );
  }
//  toggle burger menu
  const toggleStatus = () => {
    setStatus(!status);
  }

  AsyncStorage.multiGet(['userName', 'userEmail', 'userToken'], (err, items) => {
    setStore({
      ...store,
      userName: items[0][1],
      userEmail: items[1][1],
      userMobile: items[2][1]
    })
  });
  return (
    <View style={styles.container}>
      <Appbar.Header style={{ backgroundColor: '#556EF2', }}>
        <Appbar.Action icon="menu" color={"blue"} size={30}
          onPress={() => toggleStatus()} />
        <Appbar.Content title="HomeScreen"
          titleStyle={{
            fontSize: 14, fontWeight: "bold",
            color: 'white'
          }} />
      </Appbar.Header>
      <View>
      {/* burgermenu created */}
        {status == true ?
          <View style={styles.burgermenu}>

            <View style={{ padding: 10 }}>
              <Text style={styles.text}>
                Name: {store.userName}
              </Text>
              <Caption style={styles.text}>
                Email: {store.userEmail}
              </Caption>
              <Caption style={styles.text}>
                Mobile: {store.userMobile}
              </Caption>
            </View>
            <Text style={{ padding: 30, fontSize: 15, fontWeight: 'bold' }} onPress={() => signOut()}>
              Log out
              </Text>
          </View> : null
        }

       {/* Name,email,mobile data shown */ }
        <View style={{ padding:2, alignSelf: 'center' }}> 
          <Text style={styles.text}>
            Name: {store.userName}
          </Text>
          <Caption style={styles.text}>
            Email: {store.userEmail}
          </Caption>
          <Caption style={styles.text}>
            Mobile: {store.userMobile}
          </Caption>
        </View>

        <FlatList
          data={list}
          renderItem={({ item, index }) => renderBay({ item, index })}
          keyExtractor={(item, index) => item + index}
        />
      </View>
    </View>

  );
}
export default HomeScreen;
const { width } = Dimensions.get("screen");
const width_space = width / 3;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
    flex: 1
  },
  text:{ fontWeight: "bold", fontSize: 11, color: "black",padding:5 },
  burgermenu:{ height: 900, backgroundColor: 'white', width: 230, position: 'absolute', zIndex: 1, alignItems: 'flex-start' },

  bayRow: {
    width: width_space,
    marginBottom: 30,
  },
  slots: {
    justifyContent: "center",
    alignItems: "center",
    padding: 20,
    backgroundColor: 'grey'
  }
});