import React, { useContext } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    TextInput,
    Platform,
    StyleSheet,
    Alert,
    StatusBar,
    Dimensions
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Feather from 'react-native-vector-icons/Feather';
import Animated from 'react-native-reanimated';
import { useTheme } from 'react-native-paper';
import { AuthContext } from '../components/context';
import { ScrollView } from 'react-native-gesture-handler';
const SignInScreen = ({ navigation }) => {

    const [data, setData] = React.useState({
        name: '',
        email: '',
        mobile: '',
        password: '',
        check_textInputChange1: false,
        check_textInputChange2: false,
        check_textInputChange3: false,
        secureTextEntry: true,
        isValidUser1: true,
        isValidUser2: true,
        isValidUser3: true,
        isValidPassword: true,
    });

    const { colors } = useTheme();
    const { signIn } = React.useContext(AuthContext);
    
    const name_format = /^[A-Za-z]+((\s)?((\'|\-|\.)?([A-Za-z])+))*$/;
    const textInputChange1 = (val) => {
        if (val.trim().length !== 0 && name_format.test(val) == true) {
            setData({
                ...data,
                name: val,
                check_textInputChange1: true,
                isValidUser1: true
            });
        } else {
            setData({
                ...data,
                name: val,
                check_textInputChange1: false,
                isValidUser1: false
            });
        }
    }
    const email_format = /^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$/;

    const textInputChange2 = (val) => {
        if (val.trim().length !== 0 && email_format.test(val) == true) {
            setData({
                ...data,
                email: val,
                check_textInputChange2: true,
                isValidUser2: true
            });
        } else {
            setData({
                ...data,
                email: val,
                check_textInputChange2: false,
                isValidUser2: false
            });
        }
    }

    const textInputChange3 = (val) => {
        if (val.trim().length >= 10 && val.trim().length <= 10) {
            setData({
                ...data,
                mobile: val,
                check_textInputChange3: true,
                isValidUser3: true
            });
        } else {
            setData({
                ...data,
                mobile: val,
                check_textInputChange3: false,
                isValidUser3: false
            });
        }
    }
    const password_format = /^((?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%?=*&]).{8,20})+$/;

    const handlePasswordChange = (val) => {
        if (val.trim().length !== 0 && password_format.test(val) == true) {
            setData({
                ...data,
                password: val,
                isValidPassword: true
            });
        } else {
            setData({
                ...data,
                password: val,
                isValidPassword: false
            });
        }
    }

    const updateSecureTextEntry = () => {
        setData({
            ...data,
            secureTextEntry: !data.secureTextEntry
        });
    }

// validation for particular textfield
    const handleValidUser1 = (val) => {
        if (val.trim().length !== 0 && name_format.test(val) == true) {
            setData({
                ...data,
                isValidUser1: true
            });
        } else {
            setData({
                ...data,
                isValidUser1: false
            });
        }
    }

    const handleValidUser2 = (val) => {
        if (val.trim().length !== 0 && email_format.test(val) == true) {
            setData({
                ...data,
                isValidUser2: true
            });
        } else {
            setData({
                ...data,
                isValidUser2: false
            });
        }
    }


    const handleValidUser3 = (val) => {
        if (val.trim().length !== 0 && val.trim().length >= 10 && val.trim().length <= 10) {
            setData({
                ...data,
                isValidUser3: true
            });
        } else {
            setData({
                ...data,
                isValidUser3: false
            });
        }
    }
    
//    validation for login
    const loginHandle = (mobile, email, name, password) => {
        const foundUser1 = {
            mobile, email, name,password
        }
        if (email_format.test(foundUser1.email)==true && name_format.test(foundUser1.name)==true && password_format.test(foundUser1.password) ==true && (foundUser1.mobile.trim().length>=10 ||foundUser1.mobile.trim().length<=10)) {
        
            const foundUser = {
                mobile, email, name
            }
            signIn(foundUser);
            console.log('inn')
        }
        else if(data.mobile.length==0 &&data.email.length==0 &&data.name.length==0&&data.password.length==0){
            Alert.alert('Empty!', 'Field is empty.', [
                { text: 'Okay' }
            ]);
            return;
        }
        else  {
            Alert.alert('Wrong Input!', 'Please Check for errors.', [
                { text: 'Okay' }
            ]);
            return
          
           
        }
    }

    return (
        <View style={styles.container}>

            <StatusBar backgroundColor='white' barStyle="dark-content" />

            <View style={styles.header}>
                <Animated.Image
                    animation="bounceIn"
                    duraton="1500"
                    source={require('../assets/logo1.png')}
                    style={styles.logo}
                    resizeMode="cover"
                />

            </View>
            <View style={[styles.footer, { backgroundColor: colors.background }]}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Text style={styles.text_header}>Please sign in continue</Text>
                    <View style={[styles.action, { marginBottom: 15 }]}>
                        <FontAwesome
                            name="user"
                            color={colors.text}
                            size={20}
                        />
                        <TextInput
                            placeholder="Name"
                            placeholderTextColor="#666666"
                            style={[styles.textInput, {
                                color: colors.text
                            }]}
                            autoCapitalize="none"
                            keyboardType='email-address'
                            onChangeText={(val) => textInputChange1(val)}
                            onEndEditing={(e) => handleValidUser1(e.nativeEvent.text)}
                        />
                        {data.check_textInputChange1 ?
                            <Animatable.View animation="bounceIn" >
                                <Feather
                                    name="check-circle"
                                    color="green"
                                    size={20}
                                />
                            </Animatable.View>
                            : null}

                    </View>
                    {data.isValidUser1 ? null :
                        <Animatable.View animation="fadeInLeft" duration={500}>
                            <Text style={styles.errorMsg}>Enter a valid Name.</Text>
                        </Animatable.View>
                    }
                    <View style={[styles.action, { marginBottom: 15 }]}>
                        <Icon
                            name="email"
                            color={colors.text}
                            size={20}
                        />
                        <TextInput
                            placeholder="Email"
                            placeholderTextColor="#666666"
                            style={[styles.textInput, {
                                color: colors.text
                            }]}
                            autoCapitalize="none"
                            keyboardType='email-address'
                            onChangeText={(val) => textInputChange2(val)}
                            onEndEditing={(e) => handleValidUser2(e.nativeEvent.text)}
                        />
                        {data.check_textInputChange2 ?
                            <Animatable.View animation="bounceIn" >
                                <Feather
                                    name="check-circle"
                                    color="green"
                                    size={20}
                                />
                            </Animatable.View>
                            : null}

                    </View>
                    {data.isValidUser2 ? null :
                        <Animatable.View animation="fadeInLeft" duration={500}>
                            <Text style={styles.errorMsg}>Enter a valid Email Id</Text>
                        </Animatable.View>
                    }
                    <View style={[styles.action, { marginBottom: 30 }]}>
                        <FontAwesome
                            name="phone"
                            color={colors.text}
                            size={20}
                        />
                        <TextInput
                            placeholder="Your Phone No"
                            placeholderTextColor="#666666"
                            style={[styles.textInput, {
                                color: colors.text
                            }]}
                            maxLength={10}
                            autoCapitalize="none"
                            keyboardType="phone-pad"
                            onChangeText={(val) => textInputChange3(val)}
                            onEndEditing={(e) => handleValidUser3(e.nativeEvent.text)}
                        />
                        {data.check_textInputChange3 ?
                            <Animatable.View animation="bounceIn" >
                                <Feather
                                    name="check-circle"
                                    color="green"
                                    size={20}
                                />
                            </Animatable.View>
                            : null}
                    </View>
                    {data.isValidUser3 ? null :
                        <Animatable.View animation="fadeInLeft" duration={500}>
                            <Text style={styles.errorMsg}>Enter a valid mobile number.</Text>
                        </Animatable.View>
                    }
                    <View style={styles.action}>
                        <FontAwesome
                            name="lock"
                            color={colors.text}
                            size={20}
                        />
                        <TextInput
                            placeholder="Your Password"
                            placeholderTextColor="#666666"
                            secureTextEntry={data.secureTextEntry ? true : false}
                            style={[styles.textInput, {
                                color: "white", fontWeight: "400"
                            }]}
                            autoCapitalize="none"
                            onChangeText={(val) => handlePasswordChange(val)}
                        />
                        <TouchableOpacity
                            onPress={updateSecureTextEntry}
                        >
                            {data.secureTextEntry ?
                                <FontAwesome5
                                    name="eye-slash"
                                    color="grey"
                                    size={20}
                                />
                                :
                                <FontAwesome5
                                    name="eye"
                                    color="grey"
                                    size={20}
                                />
                            }
                        </TouchableOpacity>
                    </View>
                    {data.isValidPassword ? null :
                        <Animatable.View animation="fadeInLeft" duration={500}>
                            <Text style={styles.errorMsg}>Password must be 1Uppercase,1 symbol, 1 number and more then 8 characters .</Text>
                        </Animatable.View>
                    }

                    <View style={styles.button}>
                        <TouchableOpacity
                            style={styles.signIn}
                            onPress={() => { loginHandle(data.mobile, data.email, data.name, data.password,) }}
                        >
                            <LinearGradient
                                colors={['#556EF2', '#556EF2']}
                                style={styles.signIn}
                            >
                                <Text style={[styles.textSign, {
                                    color: '#fff'
                                }]}>Login</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        </View>
    );
};

export default SignInScreen;
const { height } = Dimensions.get("screen");
const height_logo = height * 0.28;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#556EF2'
    },
    header: {
        flex: 1,
        justifyContent: 'flex-end',
        paddingHorizontal: 20,
        justifyContent: "center",
        alignItems: "center"
    },
    footer: {
        flex: 5,
        backgroundColor: '#fff',
        // borderTopLeftRadius: 30,
        // borderTopRightRadius: 30,
        paddingHorizontal: 20,
        paddingVertical: 20
    },
    logo: {
        width: 100,
        height: 100,
        borderRadius: 50,

    },
    text_header: {
        color: 'white',
        fontWeight: '500',
        fontSize: 20,
        fontFamily: "Avenir",
        marginBottom: 30
    },
    text_footer: {
        color: '#05375a',
        fontSize: 20
    },
    action: {
        marginTop: 20,
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
    actionError: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#FF0000',
        paddingBottom: 5
    },
    textInput: {
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#05375a',
    },
    errorMsg: {
        color: '#FF0000',
        fontSize: 14,
    },
    button: {
        alignItems: 'center',
        marginTop: 50
    },
    signIn: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        marginHorizontal: 50
    },
    textSign: {
        fontSize: 18,
        fontWeight: 'bold'
    }
});
